import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {IdeaService} from '../../../services/idea.service';
import {Router} from '@angular/router';
import {GOALS} from '../../../model/goal';

const urlRegex = '(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?';

@Component({
  selector: 'app-new-idea',
  templateUrl: './new-idea.component.html',
  styleUrls: ['./new-idea.component.scss']
})
export class NewIdeaComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private ideaService: IdeaService, private router: Router) {
  }

  ideaForm: FormGroup;

  sending = false;
  imageLoadError = false;
  imageLoaded = false;
  errorMessages = [];
  goals = GOALS;
  thumbnailUrl = '';

  getFormValidationErrors(): (string[] | any[])[] {
    return Object.keys(this.ideaForm.controls).map(key => {

      const controlErrors: ValidationErrors = this.ideaForm.get(key).errors;
      if (controlErrors != null) {
        return Object.keys(controlErrors).map(keyError => {
          return 'Key control: ' + key + ', keyError: ' + keyError + ', err value: ' + controlErrors[keyError];
        });
      } else {
        return [];
      }
    });
  }

  ngOnInit(): void {
    this.ideaForm = this.formBuilder.group(
      {
        title: [null, Validators.required],
        author: [null, Validators.required],
        goal: [null, Validators.required],
        description: [null, [
          Validators.required,
          Validators.maxLength(500)
        ]],
        thumbnailUrl: [null, [
          Validators.required
        ]]
      }
    );
  }

  onImageError(): boolean {
    this.imageLoaded = false;
    this.imageLoadError = true;
    return false;
  }

  onImageLoad(): boolean {
    this.imageLoaded = true;
    this.imageLoadError = false;
    return false;
  }

  onSubmit(): void {
    this.sending = true;
    const controls = this.ideaForm.controls;
    this.ideaService.add({
        author: controls.author.value,
        description: controls.description.value,
        goal: controls.goal.value,
        id: 0,
        likeCount: 0,
        thumbnailUrl: controls.thumbnailUrl.value,
        title: controls.title.value
      }
    ).subscribe(() => {
      this.errorMessages = [];
      this.sending = false;
      this.router.navigate(['idea-new/success']);
    }, error => {
      if (error) {
        this.errorMessages = [error.message];
      }
      this.sending = false;
    });
  }
}
