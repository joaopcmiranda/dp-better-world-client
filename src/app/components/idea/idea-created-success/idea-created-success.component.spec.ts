import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IdeaCreatedSuccessComponent } from './idea-created-success.component';

describe('IdeaCreatedSuccessComponent', () => {
  let component: IdeaCreatedSuccessComponent;
  let fixture: ComponentFixture<IdeaCreatedSuccessComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IdeaCreatedSuccessComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IdeaCreatedSuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
