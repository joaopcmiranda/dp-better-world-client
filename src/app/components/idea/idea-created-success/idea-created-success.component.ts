import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-idea-created-success',
  templateUrl: './idea-created-success.component.html',
  styleUrls: ['./idea-created-success.component.scss']
})
export class IdeaCreatedSuccessComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
