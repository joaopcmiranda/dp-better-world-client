export interface Idea {
  id: number;
  title: string;
  author: string;
  goal: string;
  likeCount: number;
  description: string;
  thumbnailUrl: string;
}
