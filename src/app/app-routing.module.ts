import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './components/home/home.component';
import {ViewIdeaComponent} from './components/idea/view/view-idea.component';
import {NewIdeaComponent} from './components/idea/new-idea/new-idea.component';
import {IdeaCreatedSuccessComponent} from "./components/idea/idea-created-success/idea-created-success.component";

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: HomeComponent
  },
  {
    path: 'idea/:ideaId',
    component: ViewIdeaComponent
  },
  {
    path: 'idea-new',
    component: NewIdeaComponent
  },
  {
    path: 'idea-new/success',
    component: IdeaCreatedSuccessComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
